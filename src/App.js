import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Container } from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';


import NavBar from './components/NavBar';
import Home from './pages/home';
import Products from './pages/products';
import Register from './pages/register';
import Login from './pages/login';
import AddProduct from './pages/addProduct';
import UpdateProduct from './pages/updateProduct';
import ViewProduct from './pages/viewProduct';
import Checkout from './pages/checkout';

import { UserProvider } from './userContext'

export default function App() {

  const [ user, setUser ] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true",
    isLoggedIn: localStorage.getItem('isLoggedIn') === "true"
    
  })

  const [storedProductData, setStoredProductData] = useState({
    productId: localStorage.getItem('prodId'),
    productName: localStorage.getItem('prodName'),
    productDescription: localStorage.getItem('prodDescription'),
    productPrice: localStorage.getItem('prodPrice'),
  })

  function unsetUser() {
    localStorage.clear();
  }

  function unsetStoredProductData() {
    localStorage.removeItem(storedProductData.productId);
    localStorage.removeItem(storedProductData.productName);
    localStorage.removeItem(storedProductData.productDescription);
    localStorage.removeItem(storedProductData.productPrice);
  }
  
  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser, storedProductData, setStoredProductData, unsetStoredProductData }}>
        <Router>
          <NavBar />
              <Switch>
                <Route exact path="/" component={Home} />
                <Container>
                  <Route exact path="/products" component={Products} />
                  <Route exact path="/addproduct" component={AddProduct} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/updateproduct/:productId" component={UpdateProduct} />
                  <Route exact path="/viewproduct/:productId" component={ViewProduct} />
                  <Route exact path="/checkout" component={Checkout} />
                </Container>
              </Switch>
        </Router>
      </UserProvider>
    </>
  )
}