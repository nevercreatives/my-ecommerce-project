import React, { useState, useEffect, useContext } from	'react';
import { Card, CardGroup, CardColumns, Button, Row, Col, Container, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import sampleImg from '../images/products/ps4-controller.jpg';
import UserContext from '../userContext';
import { baseURL } from '../baseURL'
import '../App.css';

export default function Product({productProp}) {

	const { user, storedProductData, setStoredProductData, unsetStoredProductData } = useContext(UserContext);

	const renderTooltip = (props) => (
	  <Tooltip id="button-tooltip" {...props}>
	    View Product
	  </Tooltip>
	);

	function productDataToStore(productId) {

		if(productProp._id === productId) {
			localStorage.setItem('prodId', productProp._id);
			localStorage.setItem('prodName', productProp.name);
			localStorage.setItem('prodDescription', productProp.description);
			localStorage.setItem('prodPrice', productProp.price);
		}
	}

	const token = localStorage.getItem('token');

	function addToCart(productId) {
		console.log(productId)

		// order/checkout
		fetch(`${baseURL}/order`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				purchasedProduct: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			const {totalAmount, purchasedProduct, userId} = data;

			console.log(totalAmount);
			console.log(purchasedProduct);
			console.log(userId);
		})
	}

	function deleteFromCart(productId) {
		console.log(productId)

		// order/checkout
		fetch(`${baseURL}/order/${productId}`, {
			method: "DELETE",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// const {totalAmount, purchasedProduct, userId} = data;

			console.log(`here is the updated data: ${data}`);

			// console.log(totalAmount);
			// console.log(purchasedProduct);
			// console.log(userId);
		})
	}

	return (
		<Col >
			<CardGroup className=" mb-4">
				<Card style={{ width: '18rem' }} key={productProp._id} className="bg-purple font-pink product-card-shadow" border="info">
				  <Card.Img variant="top" src={sampleImg} />
				  <Card.ImgOverlay className="text-right">
				  	<OverlayTrigger
				  	    placement="right"
				  	    delay={{ show: 10, hide: 10 }}
				  	    overlay={renderTooltip}
				  	  >
				  	    <Link to={{pathname: `/viewproduct/${productProp._id}`}} className="btn btn-primary btn-sm font-300 font-weight-bold" onClick={() => productDataToStore(productProp._id)} >. . .</Link>
				  	  </OverlayTrigger>
				  </Card.ImgOverlay>
				  <Card.Body>
				    <Card.Title className="font-400">{productProp.name}</Card.Title>
				    <Card.Text className="font-300 paragraph-text-limit">
				      {productProp.description}
				    </Card.Text>
				    <Card.Text className="font-300">
				      Php {productProp.price}
				    </Card.Text>
				  </Card.Body>
				  {
				  	user.email
				  	? <Card.Body>
						  	<Row className="text-center">
						  		<Col>
						  			<Button className="bg-light-blue font-purple font-400" block onClick={() => deleteFromCart(productProp._id)}>Deletu</Button>
						  		</Col>
						  		<Col>
						  			<Button className="bg-light-blue font-purple font-400" block onClick={() => addToCart(productProp._id)}>Addu</Button>
						  		</Col>
						  	</Row>
						  </Card.Body>
						: <Card.Body className="text-center">
						  	<Link to='../register' className="btn bg-light-blue font-purple font-400">Register to Order</Link>
				  		</Card.Body>
				  }
				</Card>
			</CardGroup>
		</Col>
		
	)
}