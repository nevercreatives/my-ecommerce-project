import React, { useState, useEffect, useContext } from	'react';
import { Card, Button, Row, Col, Container, OverlayTrigger, Tooltip, Table, Image } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import sampleImg from '../images/products/ps4-controller-dark.jpg';
import UserContext from '../userContext';
import { baseURL } from '../baseURL'
import '../App.css';

export default function Order({orderProps}) {
	console.log(`i am a props: ${orderProps.name}`)
	return (
			<tr>
			  <td>
			  		<Image src={sampleImg} className="product-img-width" rounded fluid />
			  </td>
			  <td>{orderProps.name}</td>
			  <td>{orderProps.price}</td>
			</tr>
	)
}