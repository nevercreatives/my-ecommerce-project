import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container } from 'react-bootstrap';
import logo from '../images/zel-favicon-1.svg';
import '../App.css';

// import { NavLink, Link } from 'react-router-dom';

export default function Footer() {
	return (
		<>
			<Navbar bg="light" expand="lg">
			  <Container>
			    <Navbar.Brand href="#home" className="font-400 font-pink">
			    	<img
			    	  alt=""
  	          src={logo}
  	          width="30"
  	          height="30"
  	          className="d-inline-block align-top"
			    	        />{' '}
			    	<span className="font-pink">Zel's Fashion Boutique</span>
			    </Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			      <Nav className="me-auto font-300">
			        <Nav.Link href="#home">
			        	<span className="font-pink">Home</span>
			        </Nav.Link>
			        <Nav.Link href="#home">
			        	<span className="font-pink">Products</span>
			        </Nav.Link>
			        <Nav.Link href="#home">
			        	<span className="font-pink">Register</span>
			        </Nav.Link>
			        <Nav.Link href="#home">
			        	<span className="font-pink">Login</span>
			        </Nav.Link>
			      </Nav>
			    </Navbar.Collapse>
			  </Container>
			</Navbar>
		</>
	)
}