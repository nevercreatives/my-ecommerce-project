import React, { useContext } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import logo from '../images/zel-favicon-1.svg';
import '../App.css';

import { NavLink, Link } from 'react-router-dom';
import UserContext from '../userContext';

export default function NavBar() {

	const { user, unsetUser, setUser, unsetStoredProductData } = useContext(UserContext);

	function logout() {
		unsetUser();
		setUser({
			email: null,
			isAdmin: null
		})
		window.location.replace('/login');
	}

	return (
		<>

			<Navbar collapseOnSelect expand="lg" bg="light">
			  <Container>
			    <Navbar.Brand as={ Link } to="/" className="font-400 font-pink">
			    	<img
			    	  alt=""
  	          src={logo}
  	          width="30"
  	          height="30"
  	          className="d-inline-block align-top"
			    	        />{' '}
			    	<span className="font-pink">Zel's Collection</span>
			    </Navbar.Brand>
			    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			    <Navbar.Collapse id="responsive-navbar-nav">
			      <Nav className="me-auto font-300">
			        <Nav.Link as={ NavLink } to="/">
			        	<span className="font-pink">Home</span>
			        </Nav.Link>
			        <Nav.Link as={ NavLink } to="/products">
			        	<span className="font-pink">Products</span>
			        </Nav.Link>
			        {
			        	user.isAdmin
			        	? <>
										<Nav.Link as={NavLink} to="/addproduct">
											<span className="font-pink">Add Products</span>
										</Nav.Link>
										<Nav.Link onClick={logout}>
											<span className="font-pink">Logout</span>
										</Nav.Link>
									</>
								: user.email
									? <>
											<Nav.Link as={NavLink} to="/checkout">
												<span className="font-pink">Checkout</span>
											</Nav.Link>
											<Nav.Link onClick={logout}>
												<span className="font-pink">Logout</span>
											</Nav.Link>
										</>
									: <>
											<Nav.Link as={ NavLink } to="/register">
												<span className="font-pink" onClick={() => unsetUser()}>Register</span>
											</Nav.Link>
											<Nav.Link as={ NavLink } to="/login" onClick={() => unsetUser()}>
												<span className="font-pink">Login</span>
											</Nav.Link>
										</>
			        }
			        
			      </Nav>
			    </Navbar.Collapse>
			  </Container>
			</Navbar>
		</>
	)
}