import React, { useContext} from 'react';
import { Jumbotron, Button, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';
import '../App.css';

export default function Banner() {

	const { user } = useContext(UserContext);

	return (
		<>
			<Jumbotron className="bg-img mb-0">
				<Container>
					<h1 className="font-500">
						<span className="font-pink">Want PS4 Games? We got them all!</span>
					</h1>
					<p className="font-300">
						<span className="font-pink">Got cash? Name it and we have it!</span>
					</p>
					{
						user.isAdmin
						? <Link to='/products' className="btn btn-warning font-300">Admin Dashboard</Link>
						: <Link to='/products' className="btn btn-warning font-300">Order here!</Link>
					}
				</Container>
			</Jumbotron>
		</>
		
	)
}