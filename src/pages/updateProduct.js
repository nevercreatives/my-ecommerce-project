import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import '../App.css';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import { baseURL } from '../baseURL';

export default function UpdateProduct() {

	const { user, storedProductData, setStoredProductData, unsetStoredProductData } = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState("");
	const [willRedirect, setWillRedirect] = useState("");

	const productId = storedProductData.productId;
	const productName = storedProductData.productName;
	const productDescription = storedProductData.productDescription;
	const productPrice = storedProductData.productPrice;

	useEffect(() => {
		setName(productName);
		setDescription(productDescription);
		setPrice(productPrice);
	}, [])

	function updateProd(e) {
		e.preventDefault();

		const token = localStorage.getItem('token');

		fetch(`${baseURL}/allproducts/${productId}`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			setName(localStorage.setItem('prodName', data.name));
			setDescription(localStorage.setItem('prodDescription', data.description));
			setPrice(localStorage.setItem('prodPrice', data.price));

			if(data.message) {
				Swal.fire({
					icon: "error",
					title: "Unable to update the product",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: `Successfully Updated: ${data.name}`,
					text: `You have updated: ${data.name}`
				})
			setWillRedirect(true);
			}
		})
		// setName("");
		// setDescription("");
		// setPrice("");
	}

	return (
		user.isAdmin || willRedirect
		?	<Container className="mt-5 mb-5"> 
				<Row className="full-width-view justify-content-lg-center">
					<Col md={{ span: 9, offset: 1 }}>
						<h1 className="font-400 text-center font-pink"> Update Product</h1>
						<Form onSubmit={e => updateProd(e)}>

							<Form.Group controlId="formBasicProductName">
								<Form.Label className="font-300 font-pink">Product Name</Form.Label>
								<Form.Control className="font-300" type="text" placeholder={productName} value={name} onChange={e => {setName(e.target.value)}}  />
							</Form.Group>

							<Form.Group controlId="formBasicProductDescription">
								<Form.Label className="font-300 font-pink">Product Description</Form.Label>
								<Form.Control className="font-300" type="text" placeholder={productDescription} value={description} onChange={e => {setDescription(e.target.value)}}  />
							</Form.Group>

							<Form.Group controlId="formBasicProductPrice">
								<Form.Label className="font-300 font-pink">Product Price</Form.Label>
								<Form.Control className="font-300" type="text" placeholder={productPrice} value={price} onChange={e => {setPrice(e.target.value)}}  />
							</Form.Group>
							<Button variant="primary" type="submit">
								<span className="font-300">Submit</span>
							</Button>
						</Form>
					</Col>
				</Row>
			</Container>
		: <Redirect to='/' />
	)
}