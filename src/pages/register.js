import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import '../App.css';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import { Redirect } from 'react-router-dom';
import { baseURL } from '../baseURL';

export default function Register() {

	const { user } = useContext(UserContext);

	const[firstName, setFirstName] = useState("");
	const[lastName, setLastName] = useState("");
	const[email, setEmail] = useState("");
	const[mobileNo, setMobileNo] = useState("");
	const[password, setPassword] = useState("");
	const[confirmPassword, setConfirmPassword] = useState("");
	const[address, setAddress] = useState("");

	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false);
	const [redirectToHome, setRedirectToHome] = useState(false);

	useEffect(() => {
		if(user.isLoggedIn) {
			setRedirectToHome(true);
		} else {
				if (
					(firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && address !== "" && password !== "" && confirmPassword !== "") &&
					(firstName !== null && lastName !== null && email !== null && mobileNo !== null && address !== null && password !== null && confirmPassword !== null) &&
					(firstName !== undefined && lastName !== undefined && email !== undefined && mobileNo !== undefined && address !== undefined && password !== undefined && confirmPassword !== undefined) &&
					(password === confirmPassword) && (mobileNo.length === 11)
				) {
					setIsActive(true);
				} else {
					setIsActive(false);
				}
		}

	}, [firstName, lastName, email, mobileNo, address, password, confirmPassword, user]);

	function registerUser(e) {
		e.preventDefault();

		fetch(`${baseURL}/register`, {
			method: 'POST',
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				address: address,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			
			// test if i can get token when registering
			console.log(data);

			if(data.message) {
				Swal.fire({
					icon: "error",
					title: "Try Again",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "You can now start addu tu cartur"
				})

				setWillRedirect(true);
			}
		})
		// set all fields to empty
		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setAddress("");
		setPassword("");
		setConfirmPassword("");
	}

	return (
		redirectToHome
		? <Redirect to="/" />
		: willRedirect
			? <Redirect to="/login" />
			: <Container className="mt-5 mb-5">
					<Row className="full-width-view justify-content-lg-center">
						<Col md={{ span: 9, offset: 1 }}>
							<h1 className="font-400 font-pink text-center"> Register </h1>
							<Form onSubmit={(e) => registerUser(e)}>

								<Row>
									<Col xs={12} md={6}>
										<Form.Group  controlId="formBasicFirstName">
										  <Form.Label  className="font-300 font-pink">First Name</Form.Label>
										  <Form.Control autoComplete="off" className="font-300" type="text" placeholder="Juan" value={firstName} onChange={(e) => {
										  		setFirstName(e.target.value)
										  		console.log(`test ${firstName}`)}} />
										</Form.Group>
									</Col>

									<Col xs={12} md={6}>
										<Form.Group controlId="formBasicLastName">
										  <Form.Label className="font-300 font-pink">Last Name</Form.Label>
										  <Form.Control autoComplete="off" className="font-300" type="text" placeholder="Masipag" value={lastName} onChange={(e) => {
										  		setLastName(e.target.value)
										  		console.log(`test ${lastName}`)}} />
										</Form.Group>
									</Col>
								</Row>

							  <Form.Group  controlId="formBasicEmail">
							    <Form.Label className="font-300 font-pink">Email address</Form.Label>
							    <Form.Control autoComplete="off" className="font-300" type="email" placeholder="Enter email" value={email} onChange={(e) => {
										  		setEmail(e.target.value)
										  		console.log(`test ${email}`)}} />
							    <Form.Text className="text-muted font-200">
							      We'll never share your email with anyone else.
							    </Form.Text>
							  </Form.Group>

							  <Form.Group  controlId="formBasicMobileNo">
							    <Form.Label className="font-300 font-pink">Mobile No</Form.Label>
							    <Form.Control autoComplete="off" className="font-300" type="text" placeholder="09151234567" value={mobileNo} onChange={(e) => {
							    		setMobileNo(e.target.value)
							    		console.log(`test ${mobileNo}`)}} />
							  </Form.Group>

							  <Form.Group  controlId="formBasicAddress">
							    <Form.Label className="font-300 font-pink">Address</Form.Label>
							    <Form.Control autoComplete="off" className="font-300" type="text" placeholder="Place you call home" value={address} onChange={(e) => {
							    		setAddress(e.target.value)
							    		console.log(`test ${address}`)}} />
							  </Form.Group>

							  <Form.Group controlId="formBasicPassword">
							    <Form.Label className="font-300 font-pink">Password</Form.Label>
							    <Form.Control autoComplete="off" className="font-300" type="password" placeholder="Password" value={password} onChange={(e) => {
										  		setPassword(e.target.value)
										  		console.log(`test ${password}`)}} />
							  </Form.Group>

							  <Form.Group controlId="formBasicConfirmPassword">
							    <Form.Label className="font-300 font-pink">Confirm Password</Form.Label>
							    <Form.Control autoComplete="off" className="font-300" type="password" placeholder="Confirm Password" value={confirmPassword} onChange={(e) => {
										  		setConfirmPassword(e.target.value)
										  		console.log(`test ${confirmPassword}`)}} />
							  </Form.Group>
							  {
							  	isActive
							  	? <Button className="font-300 mb-5 submit-btn-shadow" variant="primary" type="submit">
							    		Submit
							  		</Button>
							  	: <Button className="font-300 mb-5 submit-btn-shadow" variant="primary" disabled>
							    		Submit
							 			</Button>
							  }

							</Form>
						</Col>
					</Row>
				</Container>
	)
}