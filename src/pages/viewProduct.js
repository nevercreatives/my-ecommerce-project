import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container, Image } from 'react-bootstrap';
import '../App.css';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import sampleImg from '../images/products/ps4-controller.jpg';

export default function ViewProduct() {

	const { user, storedProductData, setStoredProductData, unsetStoredProductData } = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	const productId = storedProductData.productId;
	const productName = storedProductData.productName;
	const productDescription = storedProductData.productDescription;
	const productPrice = storedProductData.productPrice;

	useEffect(() => {
		setName(productName);
		setDescription(productDescription);
		setPrice(productPrice);
	}, [])

	return (
		user.isAdmin
		? <Redirect to="/products" />
		: user.email || !user.email
			? <Row className="mt-5 mb-5">
					<Col lg={6} >
						<Image src={sampleImg} fluid rounded></Image>
					</Col>
					<Col lg={6} >
						<h1 className="font-400 font-pink">{productName}</h1>
						<p className="font-300 font-purple">{productDescription}</p>
						<h5 className="font-300 font-purple">{productPrice}</h5>
						<h6 className="font-300 font-purple">stocks left here</h6>
					</Col>
				</Row>
			: <> </>
			
	)
}