import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import { Redirect } from 'react-router-dom';
import { baseURL } from '../baseURL';

export default function Login() {

	const { user, setUser, unsetStoredProductData } = useContext(UserContext); 

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false);
	const [redirectToHome, setRedirectToHome] = useState(false);

	useEffect(() => { 
		if(user.isLoggedIn) {
			setRedirectToHome(true);
		} else {
				if (
					(email !== "" && password !== "" && confirmPassword !== "") &&
					(email !== null && password !== null && confirmPassword !== null) &&
					(email !== undefined && password !== undefined && confirmPassword !== undefined) &&
					(password === confirmPassword)
				) {
					setIsActive(true);
				} else {
					setIsActive(false);
				}
		}
		
	}, [email, password, confirmPassword, user]);

	function loginUser(e) {
		e.preventDefault();

		fetch(`${baseURL}/login`, {
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message) {
				Swal.fire({
					icon: "error",
					title: "Login Failed.",
					text: data.message
				})
			} else {
				localStorage.setItem('token', data.accessToken);

				fetch(`${baseURL}/profile`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then( res => res.json())
				.then( data => {

					localStorage.setItem('email', data.email);
					localStorage.setItem('isAdmin', data.isAdmin);
					localStorage.setItem('isLoggedIn', "true");
					setUser({
						email : data.email,
						isAdmin: data.isAdmin,
						isLoggedIn: true
					});

					setWillRedirect(true);

					Swal.fire({
						icon: "success",
						title: "Login Successfully!",
						text: `Congratulations, ${data.firstName}!`
					})
				})
			}
		})
		setEmail("");
		setPassword("");
		setConfirmPassword("");
	}

	return (
		redirectToHome
		? <Redirect to="/" />
		: user.email || willRedirect
			? <Redirect to='/products' />
			: <Container className="mt-5 mb-5">
							<Row className="full-width-view justify-content-md-center">
								<Col md={{ span: 9, offset: 0 }} lg={{ span: 6, offset: 0 }}>
									<Form onSubmit={(e) => loginUser(e)}>
										<h1 className="font-400 font-pink text-center"> Login </h1>
									  <Form.Group  controlId="formBasicEmail">
									    <Form.Label className="font-300 font-pink">Email address</Form.Label>
									    <Form.Control autoComplete="off" className="font-300" type="email" placeholder="Enter email" value={email} onChange={(e) => {setEmail(e.target.value)}} />
									  </Form.Group>

									  <Form.Group controlId="formBasicPassword">
									    <Form.Label className="font-300 font-pink">Password</Form.Label>
									    <Form.Control className="font-300" type="password" placeholder="Password" value={password} onChange={(e) => {setPassword(e.target.value)}} />
									  </Form.Group>

									  <Form.Group controlId="formBasicConfirmPassword">
									    <Form.Label className="font-300 font-pink">Confirm Password</Form.Label>
									    <Form.Control className="font-300" type="password" placeholder="Confirm Password" value={confirmPassword} onChange={(e) => {setConfirmPassword(e.target.value)}} />
									  </Form.Group>
									  
									  {
									  	isActive
									  	? <Button className="font-300 submit-btn-shadow" variant="primary" type="submit">
									    		Submit
									  		</Button>
									  	: <Button className="font-300 submit-btn-shadow" variant="primary" disabled>
									    		Submit
									 			</Button>
									  }

									</Form>
								</Col>
							</Row>
						</Container>
	)
}