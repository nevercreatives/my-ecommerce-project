import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import '../App.css';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import { baseURL } from '../baseURL';

export default function AddProduct() {
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState("");
	const [ isActive, setIsActive ] = useState(false);

	const [willRedirect, setWillRedirect] = useState(false);

	useEffect(() => {
		if (
			(name !== "" && description !== "" && price !== "") &&
			(name !== null && description !== null && price !== null) &&
			(name !== undefined && description !== undefined && price !== undefined)
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price]);

	function addNewProduct(e) {
		e.preventDefault();

		const token = localStorage.getItem('token');

		fetch(`${baseURL}/products`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			
			if(data.message) {
				Swal.fire({
					icon: "error",
					title: "Unable to add new product",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: `Successfully added ${data.name}`,
					text: `You have added ${data.name} for ${data.price} php`
				})
			setWillRedirect(true);
			}
		})
		setName("");
		setDescription("");
		setPrice("");
	}

	return (
		user.isAdmin || willRedirect
		?	<Container className="mt-5 mb-5"> 
				<Row className="full-width-view justify-content-lg-center">
					<Col md={{ span: 9, offset: 1 }}>
						<h1 className="font-400 text-center font-pink"> Add New Product</h1>
						<Form onSubmit={e => addNewProduct(e)}>

							<Form.Group controlId="formBasicProductName">
								<Form.Label className="font-300 font-pink">Product Name</Form.Label>
								<Form.Control className="font-300" type="text" placeholder="Enter Product Name" value={name} onChange={e => {setName(e.target.value)}} required />
							</Form.Group>

							<Form.Group controlId="formBasicProductDescription">
								<Form.Label className="font-300 font-pink">Product Description</Form.Label>
								<Form.Control className="font-300" type="text" placeholder="Enter Product Description" value={description} onChange={e => {setDescription(e.target.value)}} required />
							</Form.Group>

							<Form.Group controlId="formBasicProductPrice">
								<Form.Label className="font-300 font-pink">Product Price</Form.Label>
								<Form.Control className="font-300" type="text" placeholder="Enter Product Price" value={price} onChange={e => {setPrice(e.target.value)}} required />
							</Form.Group>
							{
								isActive
								? <Button variant="primary" type="submit">
										<span className="font-300">Submit</span>
									</Button>
								: <Button variant="primary" disabled>
										<span className="font-300">Submit</span>
									</Button>
							}

						</Form>
					</Col>
				</Row>
			</Container>
		: <Redirect to='/' />
	)
}