import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button, Card, Table, CardColumns, Image } from 'react-bootstrap';
import '../App.css'; 
import sampleImg from '../images/products/ps4-controller-dark.jpg';
import { baseURL } from '../baseURL';
import Order from '../components/Order';

export default function Checkout() {

	const [orderedProducts, setOrderedProducts] = useState("");
	const [productsData, setProductsData] = useState([]);

	const token = localStorage.getItem('token');

	useEffect( async () => {
		const initial = await fetch(`${baseURL}/order`, {
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})

    const initialJson = await initial.json()
    const [{purchasedProduct}] = initialJson;
    console.log(purchasedProduct) // data array

    const detailsData = purchasedProduct.map( product => {
    		console.log(`this is product.productId only: ${product.productId}`)
    		console.log(`this is product.productId: ${product.productId.name}`)
    		console.log(`this is product.productId: ${product.productId.price}`)

    		// for(let productIndex in purchasedProduct) {
    		// 	console.log(`productIndex: ${productIndex}`)
    		// }

    		let props = {
    			name: product.productId.name,
    			price: product.productId.price
    		}
    			console.log(`i am a props: ${props.name}`)

    		return (
    			<Order orderProps={props}/>
    		)
    })

    setOrderedProducts(detailsData)
	}, [])

	return (
		<Container fluid>
			<Row className="mt-5 mb-5" >
				{/* 1st col*/}
				<Col lg={{ span : 8 }} >
					<Row>
						<h1 className="font-400 font-pink">Shopping Cart</h1>
					</Row>

				{/*Table Main Row*/}
					<Row xs={1} className="font-300">
						<Table responsive="lg">
						    <thead className="font-purple">
						      <tr >
						        <th >Product</th>
						        <th>Product Name</th>
						        <th>Product Price</th>
						      </tr>
						    </thead>

						    <tbody className="font-purple">
						      {orderedProducts}
						    </tbody>
						  </Table>
					</Row>
					{/*<Row>
						<Col>
							<Button className="font-300" >Back to shop</Button>
						</Col>
						<Col className="font-purple">
							<h6>amounts</h6>
							<h6>amounts</h6>
							<h6>amounts</h6>
						</Col>
					</Row>*/}
				</Col>
			{/* 2nd col*/}
				{/*<Col lg={4}>
					<Container fluid>
						<Card
						    bg="secondary"
						    text="white"
						    className="mb-2"

						  >
						    <Card.Header>Header</Card.Header>
						    <Card.Body>
						      <Card.Title> Card Title </Card.Title>
						      <Card.Text>
						        Some quick example text to build on the card title and make up the bulk
						        of the card's content.
						      </Card.Text>
						    </Card.Body>
						  </Card>
					  </Container>
				</Col>*/}
			</Row>
		</Container>
	)
}