import React, { useState, useEffect, useContext } from 'react';
import { Container, Table, Button, Row, Col } from 'react-bootstrap';
import '../App.css';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';
import Product from '../components/Product';
import UpdateProduct from './updateProduct';
import { baseURL } from '../baseURL';

export default function Products() {

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [activeProducts, setActiveProducts] = useState([]);
	const [update, setUpdate] = useState(0);

	useEffect(() => {

		fetch(`${baseURL}/allproducts`)
		.then(res => res.json())
		.then(data => {
			setAllProducts(data.data);

			let productTemp = data.data;

			let tempArray  = productTemp.filter(product => {
				return product.isActive === true;
			})

			setActiveProducts(tempArray);

		})
	}, [update]);

	let productComponents = activeProducts.map(product => {
		return (
			<Product key={product._id} productProp={product} />
		)
	});

	function archive(productId) {
		fetch(`${baseURL}/products/archive/${productId}`, {
			method: "PUT",
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUpdate({});
		})
	}

	function activate(productId) {
		fetch(`${baseURL}/products/activate/${productId}`, {
			method: "PUT",
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUpdate({});
		})
	}

	let productRows = allProducts.map(product => {

		// make this reuseable. change params to obj and destructure inside the function
		function productDataToStore(productId) {

			if(product._id === productId) {
				localStorage.setItem('prodId', product._id);
				localStorage.setItem('prodName', product.name);
				localStorage.setItem('prodDescription', product.description);
				localStorage.setItem('prodPrice', product.price);
			}
		}

		return (
			<tr key={product._id}>
				<td className="font-300">{product._id}</td>
				<td className="font-300">{product.name}</td>
				<td className={product.isActive ? "text-success text-center font-300" : "text-danger text-center font-300"}>
					{
						product.isActive
						? "Active"
						: "Inactive"
					}
				</td>
				<td className="text-center">
					{
						product.isActive
						? <Button variant="danger" className="mx-2 font-300" onClick={() => archive(product._id)}>
								Archive
							</Button>
						: <Button variant="success" className="mx-2 font-300" onClick={() => activate(product._id)}>
								Activate
							</Button>
					}
				</td>
				<td className="font-300 text-center">
					<Link to={{
						pathname: `/updateproduct/${product._id}`
					}} className="btn btn-primary font-300" onClick={() => productDataToStore(product._id)} >Update</Link>
				</td>
			</tr>
		)
	})
		
	return (
		user.isAdmin
		? <Container className="mb-5 mt-5">
				<h1 className="text-center font-400 font-pink">Admin Dashboard</h1>
				<Table striped bordered hover >
					<thead>
						<tr className="font-purple">
							<th>ID</th>
							<th>Name</th>
							<th>Status</th>
							<th >Actions</th>
							<th >Update Product</th>
						</tr>
					</thead>
					<tbody className="font-purple">
						{productRows}
					</tbody>
				</Table>
			</Container>
		: <Container className="mt-5 mb-5">
	 			<Row sm={1} md={2} lg={3} className="">
	 				{productComponents}
	 			</Row>	
			</Container>
		
		
	)
}
